# Comment installer et configurer le client Shibboleth 3.2 pour l'UNIGE sous Windows (environnement ISIS)

* Installation du client Shibboleth à l'aide de la documentation détaillée de SWITCH:
[https://www.switch.ch/aai/guides/sp/installation/](https://www.switch.ch/aai/guides/sp/installation/)

* Configuration du client Shibboleth à l'aide de la documentation détaillée de SWITCH:
[https://www.switch.ch/aai/guides/sp/configuration/](https://www.switch.ch/aai/guides/sp/configuration/)

***Attention: L'étape "7. Register Service Provider " n'est pas à faire.***
<!-- Not necessary: the file will be downloaded and the signature is not verified

* Télécharger le fichier Metadata de l'IdP ainsi que le certificat de signature sur l'URL suivante:
[https://adfs.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml](https://adfs.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml)

* Nommer les fichiers téléchargés comme indiqué:
> Nom du fichier XML: [adfs.unige.ch_metadata.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/adfs.unige.ch_metadata.xml)
> 
> Nom du fichier de certificat: [adfs.unige.ch_signing.pem](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/adfs.unige.ch_signing.pem)

* Copier les 2 fichiers dans `C:\opt\shibboleth-sp\etc\shibboleth`
-->

* Créer une copie de sauvegarde du fichier `shibboleth2.xml` en `shibboleth2-orig.xml`

* Modifier le fichier `shibboleth2.xml` en éditant certaines parties comme indiqué:
 

> Avant:

```xml
<OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" />
```

> Après:

```xml
<OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a">
  <!-- ADFS Extension -->
  <Extensions>
    <Library path="adfs.so" fatal="true"/>
  </Extensions>
</OutOfProcess>
```

> Avant:

```xml
<InProcess>
  <ISAPI normalizeRequest="true" safeHeaderNames="true">
    <!--
    Maps IIS Instance ID values to the host scheme/name/port. The name is
    required so that the proper <Host> in the request map above is found without
    having to cover every possible DNS/IP combination the user might enter.
    When the port and scheme are omitted, the HTTP request's port and scheme are used.
    If these are wrong because of virtualization, they can be explicitly set
    (e.g. with scheme="https" port="443" ) ensure proper redirect generation.
    Fore information on https://wiki.shibboleth.net/confluence/display/SP3/ISAPI
    -->
   <Site Id="1" name="site web name"/>
  </ISAPI>
</InProcess>
```

> Après:

```xml
<InProcess>
  <Extensions>
    <Library path="adfs-lite.so" fatal="true"/>
  </Extensions>
  <ISAPI normalizeRequest="true" safeHeaderNames="true">
    <!--
    Maps IIS Instance ID values to the host scheme/name/port. The name is
    required so that the proper <Host> in the request map above is found without
    having to cover every possible DNS/IP combination the user might enter.
    When the port and scheme are omitted, the HTTP request's port and scheme are used.
    If these are wrong because of virtualization, they can be explicitly set
    (e.g. with scheme="https" port="443" ) ensure proper redirect generation.
    Fore information on https://wiki.shibboleth.net/confluence/display/SP3/ISAPI
    -->
    <Site id="1" name="site web name"/>
  </ISAPI>
</InProcess>
```

> Avant:

```xml
<SSO discoveryProtocol="SAMLDS" discoveryURL="https://wayf.switch.ch/SWITCHaai/WAYF">
  SAML2
</SSO>
```

> Après:

```xml
<SSO entityID="http://adfs.unige.ch/adfs/services/trust">
  SAML2 ADFS
</SSO>
```

> Avant:

```xml
<Logout>SAML2 Local</Logout>
```

> Après:

```xml
<Logout>SAML2 ADFS Local</Logout>
```

> Avant:

```xml
<MetadataProvider
  validate="true"
  url="http://metadata.aai.switch.ch/metadata.switchaai+idp.xml"
  backingFilePath="metadata.switchaai+idp.xml"
  reloadInterval="3600">
  <MetadataFilter
   maxValidityInterval="604800"/>
  <MetadataFilter verifyBackup="false">
    <TrustEngine
      certificate="SWITCHaaiRootCA.crt.pem"
      verifyDepth="2"
      checkRevocation="fullChain"
      policyMappingInhibit="true"
      anyPolicyInhibit="true">
      <TrustedName>SWITCHaai Metadata Signer</TrustedName>
      <PolicyOID>2.16.756.1.2.6.7</PolicyOID>
    </TrustEngine>
  </MetadataFilter>
  <MetadataFilter type="EntityRole">
    <RetainedRole>md:IDPSSODescriptor</RetainedRole>
    <RetainedRole>md:AttributeAuthorityDescriptor</RetainedRole>
  </MetadataFilter>
</MetadataProvider>
```

> Après:

```xml
<MetadataProvider type="XML"
      validate="true"
      url="https://adfs.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml"
      backingFilePath="adfs.unige.ch_metadata.xml"
      reloadInterval="3600">
    <MetadataFilter type="EntityRole">
        <RetainedRole>md:IDPSSODescriptor</RetainedRole>
        <RetainedRole>md:AttributeAuthorityDescriptor</RetainedRole>
    </MetadataFilter>
</MetadataProvider>

```

> Avant:

```xml
<AttributeExtractor validate="true" reloadChanges="false" path="attribute-map.xml"/>
```

> Après:

```xml
<AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map-switch-and-unige-combined.xml"/>
```

* Modifier la section du fichier `attribute-policy.xml`:

C'est nécessaire afin que les attributs avec une valeur contenant le suffixe @unige.ch soient correctement transmis, p.ex. uniqueID.

> Avant:

```xml
<PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="ValueRegex" regex="@"/>
        </Rule>
        <Rule xsi:type="ScopeMatchesShibMDScope"/>
</PermitValueRule>
```

> Après:

```xml
<PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="ValueRegex" regex="@"/>
        </Rule>
        <!-- <Rule xsi:type="ScopeMatchesShibMDScope"/> -->
</PermitValueRule>
```

* Copier le fichier [attribute-map-switch-and-unige-combined.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/attribute-map-switch-and-unige-combined.xml) dans `C:\opt\shibboleth-sp\etc\shibboleth`.

* Tester la configuration avec la commande `C:\opt\shibboleth-sp\sbin\shibd.exe -check`

* Redémarrer le service Shibboleth avec la commande `Restart-Service shibd_default`

* Redémarrer le service web (IIS ou apache) 

* [Soumettre une demande de déclaration de site pour la configuration SSO dans l'infrastructure UNIGE](https://support-si.unige.ch/openentry.html?tid=SRD000000004902)

