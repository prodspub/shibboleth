# Migration vers l'IdP ADFS d'un client Shibboleth pour l'UNIGE sous Solaris (environnement ISIS de test)

## 1) Préparation globale

* backup du répertoire /usr/local/unige_etc/shibboleth/

* Mise à jour du client Shibboleth avec le package compilé par Olivier Jeannin pour cette version de Solaris

* Télécharger certains fichiers de configurations en utilisant l'outil de Configuration dans la [documentation détaillée de SWITCH](https://www.switch.ch/aai/guides/sp/configuration/) ou les commandes curl suivantes:

   - [attribute-policy.xml](https://www.switch.ch/aai/docs/shibboleth/SWITCH/3.1/sp/deployment/download/customize.php/attribute-policy.xml?osType=nonwindows&hide=)

```bash
curl --output /usr/local/unige_etc/shibboleth/attribute-policy.xml 'https://www.switch.ch/aai/docs/shibboleth/SWITCH/3.1/sp/deployment/download/customize.php/attribute-policy.xml?osType=nonwindows&hide='
```


* Créer une copie de sauvegarde du fichier `shibboleth2.xml` en `shibboleth2-orig.xml`
```
cp -p /etc/shibboleth/shibboleth2.xml /etc/shibboleth/shibboleth2-orig.xml
```

* Modifier le fichier `shibboleth2.xml` en éditant certaines parties comme indiqué:
 

> Avant

```xml
<OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" />
```

> Après:

```xml
<OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" >
  <!-- ADFS Extension -->
  <Extensions>
    <Library path="/usr/local/unige_pack/shibboleth/lib/shibboleth/adfs.so" fatal="true"/>
  </Extensions>
</OutOfProcess>
<InProcess>
  <Extensions>
    <Library path="/usr/local/unige_pack/shibboleth/lib/shibboleth/adfs-lite.so" fatal="true"/>
  </Extensions>
</InProcess>
```

> Pour chaque application qui migre sur ADFS (sauf default), ajouter dans l'élément `<ApplicationOverride>` correspondant un générateur de metadata avec l'URL de base de tous les endpoints de l'application (cf. section "Service Locations" de l'ancienne définition de l'application dans le SWITCH [AAI Resource Registry](https://rr.aai.switch.ch/list_resources.php?action=search)) :

Par exemple, pour l'application `tec/deploiement` :

```xml
        <!-- Extension service that generates "approximate" metadata based on SP configuration. -->
        <Handler type="MetadataGenerator"
                     Location="/Metadata"
                     signing="false">
                <EndpointBase>https://wwwit.intranet.unige.ch/tec/deploiement/Shibboleth.sso</EndpointBase>
                <EndpointBase>https://wwwot.intranet.unige.ch/tec/deploiement/Shibboleth.sso</EndpointBase>
        </Handler>
```

> Avant: dans l'élément `<ApplicationDefaults>`

```xml
<Logout>SAML2 Local</Logout>
```

> Après:

```xml
<Logout>SAML2 ADFS Local</Logout>
```

> Tant que ce SP doit servir des applications dans SWITCH AAI (IdP Shibboleth) conserver l'élément `<MetadataProvider>` pour AAI suivant

```xml
        <!-- Download and verify AAI Test metadata -->
        <MetadataProvider type="XML"
                          validate="true"
                          url="http://metadata.aai.switch.ch/metadata.aaitest+idp.xml"
                          backingFilePath="metadata.aaitest+idp.xml"
                          reloadInterval="3600">
            <MetadataFilter type="RequireValidUntil"
                            maxValidityInterval="604800"/>
            <MetadataFilter type="Signature" verifyBackup="false">
                <TrustEngine type="StaticPKIX"
                             certificate="SWITCHaaiRootCA.crt.pem"
                             verifyDepth="2"
                             checkRevocation="fullChain"
                             policyMappingInhibit="true"
                             anyPolicyInhibit="true">
                      <TrustedName>SWITCHaai Metadata Signer</TrustedName>
                      <PolicyOID>2.16.756.1.2.6.7</PolicyOID>
               </TrustEngine>
            </MetadataFilter>
            <MetadataFilter type="EntityRoleWhiteList">
                <RetainedRole>md:IDPSSODescriptor</RetainedRole>
                <RetainedRole>md:AttributeAuthorityDescriptor</RetainedRole>
            </MetadataFilter>
        </MetadataProvider>
```

> Et ajouter un deuxième élément `<MetadataProvider>` pour ADFS (dans l'élément `<ApplicationDefaults>`)

```xml
<MetadataProvider type="XML"
                  validate="true"
                  url="https://adfsklif.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml"
                  backingFilePath="adfsklif.unige.ch_metadata.xml"
                  reloadInterval="3600">
    <MetadataFilter type="EntityRoleWhiteList">
        <RetainedRole>md:IDPSSODescriptor</RetainedRole>
        <RetainedRole>md:AttributeAuthorityDescriptor</RetainedRole>
    </MetadataFilter>
</MetadataProvider>
```

> Avant: dans l'élément `<ApplicationDefaults>`

```xml
<AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map-switch-and-unige-combined.xm"/>
```

> Après:

```xml
<AttributeExtractor type="XML"
                    validate="true"
                    reloadChanges="false"
                    path="attribute-map-switch-and-unige-adfs-combined.xml"/>
```

* Copier le fichier [attribute-map-switch-and-unige-adfs-combined.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/attribute-map-switch-and-unige-adfs-combined.xml) dans ` /usr/local/unige_etc/shibboleth/`: 
```
curl --output /usr/local/unige_etc/shibboleth/attribute-map-switch-and-unige-adfs-combined.xml 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/attribute-map-switch-and-unige-adfs-combined.xml'
```

* Modifier la section du fichier `attribute-policy.xml`:

C'est nécessaire afin que les attributs avec une valeur contenant le suffixe `@unige.ch` soient correctement transmis, p.ex. `uniqueID`.

> Avant:

```xml
<PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="ValueRegex" regex="@"/>
        </Rule>
        <Rule xsi:type="ScopeMatchesShibMDScope"/>
</PermitValueRule>
```

> Après:

```xml
<PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="ValueRegex" regex="@"/>
        </Rule>
        <!-- <Rule xsi:type="ScopeMatchesShibMDScope"/> -->
</PermitValueRule>
```


* Copier les fichiers suivants dans le répertoire `/usr/share/xml/shibboleth/`

> [addressing.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/addressing.xsd)
> 
> [catalog.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/catalog.xml)
> 
> [MetadataExchange.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/MetadataExchange.xsd)
> 
> [oasis-200401-wss-wssecurity-secext-1.0.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-secext-1.0.xsd)
> 
> [oasis-200401-wss-wssecurity-utility-1.0.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-utility-1.0.xsd)
> 
> [ws-authorization.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-authorization.xsd)
> 
> [ws-federation.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-federation.xsd)
> 
> [ws-policy.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-policy.xsd)
> 
> [ws-privacy.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-privacy.xsd)
> 
> [ws-securitypolicy-1.2.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-securitypolicy-1.2.xsd)
> 
> [WS-Trust.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/WS-Trust.xsd)

```bash
mkdir -p /usr/share/xml/shibboleth/
curl --output /usr/share/xml/shibboleth/addressing.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/addressing.xsd'
curl --output /usr/share/xml/shibboleth/catalog.xml 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/catalog.xml'
curl --output /usr/share/xml/shibboleth/MetadataExchange.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/MetadataExchange.xsd'
curl --output /usr/share/xml/shibboleth/oasis-200401-wss-wssecurity-secext-1.0.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-secext-1.0.xsd'
curl --output /usr/share/xml/shibboleth/oasis-200401-wss-wssecurity-utility-1.0.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-utility-1.0.xsd'
curl --output /usr/share/xml/shibboleth/ws-authorization.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-authorization.xsd'
curl --output /usr/share/xml/shibboleth/ws-federation.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-federation.xsd'
curl --output /usr/share/xml/shibboleth/ws-policy.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-policy.xsd'
curl --output /usr/share/xml/shibboleth/ws-privacy.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-privacy.xsd'
curl --output /usr/share/xml/shibboleth/ws-securitypolicy-1.2.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-securitypolicy-1.2.xsd'
curl --output /usr/share/xml/shibboleth/WS-Trust.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/WS-Trust.xsd'
```

* Tester la configuration Shibboleth et redémarrer le service Shibboleth
```bash 
/etc/init.d/shibboleth-sp configtest
/etc/init.d/shibboleth-sp restart
## check shibd logs:
less /usr/local/unige_pack/shibboleth/var/log/shibboleth/shibd.log
```
* Redémarrer le service Apache
```bash
/etc/init.d/httpd restart
```

* Fournir à Pierre Deshayes pour chaque application qui migre sur l'IdP ADFS, directement si urgent, ou via le formulaire "[Soumettre une demande de déclaration de site pour la configuration SSO dans l'infrastructure UNIGE](https://support-si.unige.ch/openentry.html?tid=SRD000000004902)":
   - le nom de l'application
   - l'entityID de l'application
   - l'URL pour charger le fichier metadata correspondant
   - la page racine de l'application (home)

Par exemple:
   - applicationName = PROD-TEC/DEPLOIEMENT CLU TST
   - entityID = https://wwwit.unige.ch/tec/deploiement/shibboleth
   - metadataURL = https://wwwit.intranet.unige.ch/tec/deploiement/Shibboleth.sso/Metadata
   - homeURL = https://wwwit.intranet.unige.ch/tec/deploiement/faces/home

## 2) Migrer une ou plusieurs applications sur l'IdP ADFS

> Avant la migration, demander à un utilisateur habilité de tester l'application, puis consulter les logs de transactions pour identifier quels attributs sont transmis normalement, par exemple:
```console
$ grep tec/deploiement /usr/local/unige_pack/shibboleth/var/log/shibboleth/transaction.log|tail -1
2020-12-07 22:49:43|Shibboleth-TRANSACTION.Login|https://idp-test.unige.ch/idp/shibboleth!https://wwwit.unige.ch/tec/deploiement/shibboleth!x19JZb5DsNNkzx4ktcQsOChg6C4=|_7bd565b90ca2e505ca7e3daf1862f470|https://idp-test.unige.ch/idp/shibboleth|_bd32363bda4aaf03eee94a691117d432|urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport|2020-12-07T14:58:59|affiliation(2),givenName(1),homeOrganization(1),homeOrganizationType(1),mail(1),persistent-id(1),surname(1),uniqueID(1)|AAlzZWNyZXQyODmGbkO2TwEfBNR2g/OqJgaQOx561QR/OPGObpsnACQz+otEb6aGGdvkaxGc33M0rKcCtlTaytzYTEzTx+kD6kNGjdunp/UYnfTtCG2k/fN7FXUknkuGcRlOFXQAmxnDtvwu4pB6+IJnbIemm0coC5eqTXk9s5WG|urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST||urn:oasis:names:tc:SAML:2.0:status:Success|||Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0|10.194.17.9
```

> Pour chaque application qui migre sur ADFS (sauf default), dans `/usr/local/unige_etc/shibboleth/shibboleth2.xml` dans l'élément `<ApplicationOverride>` correspondant:

Ne rien faire si il y a déjà un élément `<SSO>` avec paramètre `discoveryURL`: c'est une indication que l'application ne peut pas migrer sur l'IdP ADFS (audience ouverte à d'autres organisations).
```xml
<SSO discoveryProtocol="SAMLDS" discoveryURL="https://wayf-test.switch.ch/aaitest/WAYF">
   SAML2 SAML1
</SSO>
```

Sinon ajouter un élément `<SSO>` avec ADFS comme IdP par défaut (l'application migre sur l'IdP ADFS):
```xml
<SSO entityID="http://adfsklif.unige.ch/adfs/services/trust">
  SAML2 ADFS
</SSO>
```

* Si nécessaire, modifier la configuration du virtual host Apache correspondant à chaque application migrant sur l'IdP ADFS en enlevant la référence à l'IdP Shibboleth comme indiqué (l'IdP étant maintenant défini dans shibboleth2.xml):

> Avant, par exemple:
```apache
<Location "/myapplicationpath">
  AuthType shibboleth
  ShibRequestSetting applicationId myapplication
  ShibRequestSetting entityID https://idp-test.unige.ch/idp/shibboleth
  ShibRequestSetting requireSession true
  ShibUseHeaders On
  Require shibboleth
  Require valid-user
</Location>
```

> Après:
```apache
<Location "/myapplicationpath">
  AuthType shibboleth
  ShibRequestSetting applicationId myapplication
  ShibRequestSetting requireSession true
  ShibUseHeaders On
  Require shibboleth
  Require valid-user
</Location>
```

* Tester la configuration Shibboleth et redémarrer le service Shibboleth
```bash 
/etc/init.d/shibboleth-sp configtest
/etc/init.d/shibboleth-sp restart
## check shibd logs:
less /usr/local/unige_pack/shibboleth/var/log/shibboleth/shibd.log
```

* Tester la configuration Apache et redémarrer le service Apache
```bash
/etc/init.d/httpd configtest
/etc/init.d/httpd restart
## check apache logs modified in the last 2 minutes
find /usr/local/unige_data/httpd/logs/ -mmin -2 |xargs less

```
> Demander à un utilisateur de re-tester l'application, et consulter les logs de shibd et de transactions pour vérifier si les mêmes attributs ont bien été transmis, par exemple:
```console
$ tail -10 /usr/local/unige_pack/shibboleth/var/log/shibboleth/shibd.log
$ grep tec/deploiement /usr/local/unige_pack/shibboleth/var/log/shibboleth/transaction.log|tail -1

```


