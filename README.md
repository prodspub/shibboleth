# Comment installer et configurer le client Shibboleth pour bénéficier du SSO de l'UNIGE

Les procédures d'installations suivantes entrent dans le cadre de la mise en place du SSO de l'Université sur un site Web.

Le service lié est disponible dans le [Catalogue de Services Numériques de l'Université](https://catalogue-si.unige.ch/sso)

Certaines recommandations sont disponibles [ici](https://plone.unige.ch/distic/pub/webtools/authentification-sso-accueil).

* [Comment installer et configurer le client Shibboleth pour l'UNIGE sous Linux (environnement ISIS)](Linux.md#user-content-comment-installer-et-configurer-le-client-shibboleth-pour-lunige-sous-linux-environnement-isis)
* [Comment installer et configurer le client Shibboleth pour l'UNIGE sous Linux (environnement ISIS de test)](Linux_klif.md#user-content-comment-installer-et-configurer-le-client-shibboleth-pour-lunige-sous-linux-environnement-isis-de-test)
* [Comment installer et configurer le client Shibboleth pour l'UNIGE sous Windows (environnement ISIS)](Windows.md#comment-installer-et-configurer-le-client-shibboleth-pour-lunige-sous-windows-environnement-isis)
* [Comment installer et configurer le client Shibboleth pour l'UNIGE sous Windows (environnement ISIS de test)](Windows_klif.md#comment-installer-et-configurer-le-client-shibboleth-pour-lunige-sous-windows-environnement-isis-de-test)
* [Migration vers l'IdP ADFS d'un client Shibboleth pour l'UNIGE sous Solaris (environnement ISIS de test)](Solaris_klif.md#migration-vers-lidp-adfs-dun-client-shibboleth-pour-lunige-sous-solaris-environnement-isis-de-test)