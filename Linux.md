# Comment installer et configurer le client Shibboleth pour l'UNIGE sous Linux (environnement ISIS)

* Installation du client Shibboleth à l'aide de la documentation détaillée de SWITCH:
[https://www.switch.ch/aai/guides/sp/installation/](https://www.switch.ch/aai/guides/sp/installation/)

* Configuration du client Shibboleth à l'aide de la documentation détaillée de SWITCH:
[https://www.switch.ch/aai/guides/sp/configuration/](https://www.switch.ch/aai/guides/sp/configuration/)

***Attention: L'étape "7. Register Service Provider " n'est pas à faire.***

<!-- Not necessary: the file will be downloaded and the signature is not verified
* Télécharger le fichier Metadata de l'IdP ainsi que le certificat de signature sur l'URL suivante:
[https://adfs.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml](https://adfs.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml)

* Nommer les fichiers téléchargés comme indiqué:
> Nom du fichier XML: [adfs.unige.ch_metadata.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/adfs.unige.ch_metadata.xml)
> 
> Nom du fichier de certificat: [adfs.unige.ch_signing.pem](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/adfs.unige.ch_signing.pem)

* Copier les 2 fichiers dans `/etc/shibboleth`
```
curl --output /etc/shibboleth/adfs.unige.ch_metadata.xml 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/adfs.unige.ch_metadata.xml'
curl --output /etc/shibboleth/adfs.unige.ch_signing.pem 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/adfs.unige.ch_signing.pem'
```
-->

* Créer une copie de sauvegarde du fichier `shibboleth2.xml` en `shibboleth2-orig.xml`
```
cp -p /etc/shibboleth/shibboleth2.xml /etc/shibboleth/shibboleth2-orig.xml
```

* Modifier le fichier `shibboleth2.xml` en éditant certaines parties comme indiqué:
 

> Avant:

```xml
<OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" />
```

> Après:

```xml
<OutOfProcess tranLogFormat="%u|%s|%IDP|%i|%ac|%t|%attr|%n|%b|%E|%S|%SS|%L|%UA|%a" >
  <!-- ADFS Extension -->
  <Extensions>
    <Library path="/usr/lib/x86_64-linux-gnu/shibboleth/adfs.so" fatal="true"/>
  </Extensions>
</OutOfProcess>
<InProcess>
  <Extensions>
    <Library path="/usr/lib/x86_64-linux-gnu/shibboleth/adfs-lite.so" fatal="true"/>
  </Extensions>
</InProcess>
```

> Avant:

```xml
<SSO entityID="https://idp.example.org/idp/shibboleth" 
  discoveryProtocol="SAMLDS" discoveryURL="https://ds.example.org/DS/WAYF">
  SAML2
</SSO>
```

> Après:

```xml
<SSO entityID="http://adfs.unige.ch/adfs/services/trust">
  SAML2 ADFS
</SSO>
```

> Avant:

```xml
<Logout>SAML2 Local</Logout>
```

> Après:

```xml
<Logout>SAML2 ADFS Local</Logout>
```

> Avant:

```xml
    <!-- Download and verify SWITCHaai metadata -->
        <MetadataProvider type="XML"
                          validate="true"
                          url="http://metadata.aai.switch.ch/metadata.switchaai+idp.xml"
                          backingFilePath="metadata.switchaai+idp.xml"
                          reloadInterval="3600">
            <MetadataFilter type="RequireValidUntil"
                            maxValidityInterval="604800"/>
            <MetadataFilter type="Signature" verifyBackup="false">
                <TrustEngine type="StaticPKIX"
                             certificate="SWITCHaaiRootCA.crt.pem"
                             verifyDepth="2"
                             checkRevocation="fullChain"
                             policyMappingInhibit="true"
                             anyPolicyInhibit="true">
                      <TrustedName>SWITCHaai Metadata Signer</TrustedName>
                      <PolicyOID>2.16.756.1.2.6.7</PolicyOID>
               </TrustEngine>
            </MetadataFilter>
            <MetadataFilter type="EntityRoleWhiteList">
                <RetainedRole>md:IDPSSODescriptor</RetainedRole>
                <RetainedRole>md:AttributeAuthorityDescriptor</RetainedRole>
            </MetadataFilter>
        </MetadataProvider>
```

> Après:

```xml
<MetadataProvider type="XML"
                  validate="true"
                  url="https://adfs.unige.ch/FederationMetadata/2007-06/FederationMetadata.xml"
                  backingFilePath="adfs.unige.ch_metadata.xml"
                  reloadInterval="3600">
    <MetadataFilter type="EntityRoleWhiteList">
        <RetainedRole>md:IDPSSODescriptor</RetainedRole>
        <RetainedRole>md:AttributeAuthorityDescriptor</RetainedRole>
    </MetadataFilter>
</MetadataProvider>
```

> Avant:

```xml
<AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map.xml"/>
```

> Après:

```xml
<AttributeExtractor type="XML"
                    validate="true"
                    reloadChanges="false"
                    path="attribute-map-switch-and-unige-combined.xml"/>
```

* Copier le fichier [attribute-map-switch-and-unige-combined.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/attribute-map-switch-and-unige-combined.xml) dans `/etc/shibboleth`: 
```
curl --output /etc/shibboleth/attribute-map-switch-and-unige-combined.xml 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/attribute-map-switch-and-unige-combined.xml'
```
* Modifier la section du fichier `attribute-policy.xml`:

C'est nécessaire afin que les attributs avec une valeur contenant le suffixe @unige.ch soient correctement transmis, p.ex. uniqueID.

> Avant:

```xml
<PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="ValueRegex" regex="@"/>
        </Rule>
        <Rule xsi:type="ScopeMatchesShibMDScope"/>
</PermitValueRule>
```

> Après:

```xml
<PermitValueRule id="ScopingRules" xsi:type="AND">
        <Rule xsi:type="NOT">
            <Rule xsi:type="ValueRegex" regex="@"/>
        </Rule>
        <!-- <Rule xsi:type="ScopeMatchesShibMDScope"/> -->
</PermitValueRule>
```

* Copier les fichiers suivants dans le répertoire `/usr/share/xml/shibboleth/`

> [addressing.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/addressing.xsd)
> 
> [catalog.xml](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/catalog.xml)
> 
> [MetadataExchange.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/MetadataExchange.xsd)
> 
> [oasis-200401-wss-wssecurity-secext-1.0.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-secext-1.0.xsd)
> 
> [oasis-200401-wss-wssecurity-utility-1.0.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-utility-1.0.xsd)
> 
> [ws-authorization.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-authorization.xsd)
> 
> [ws-federation.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-federation.xsd)
> 
> [ws-policy.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-policy.xsd)
> 
> [ws-privacy.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-privacy.xsd)
> 
> [ws-securitypolicy-1.2.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-securitypolicy-1.2.xsd)
> 
> [WS-Trust.xsd](https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/WS-Trust.xsd)

```
curl --output /usr/share/xml/shibboleth/addressing.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/addressing.xsd'
curl --output /usr/share/xml/shibboleth/catalog.xml 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/catalog.xml'
curl --output /usr/share/xml/shibboleth/MetadataExchange.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/MetadataExchange.xsd'
curl --output /usr/share/xml/shibboleth/oasis-200401-wss-wssecurity-secext-1.0.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-secext-1.0.xsd'
curl --output /usr/share/xml/shibboleth/oasis-200401-wss-wssecurity-utility-1.0.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/oasis-200401-wss-wssecurity-utility-1.0.xsd'
curl --output /usr/share/xml/shibboleth/ws-authorization.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-authorization.xsd'
curl --output /usr/share/xml/shibboleth/ws-federation.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-federation.xsd'
curl --output /usr/share/xml/shibboleth/ws-policy.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-policy.xsd'
curl --output /usr/share/xml/shibboleth/ws-privacy.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-privacy.xsd'
curl --output /usr/share/xml/shibboleth/ws-securitypolicy-1.2.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/ws-securitypolicy-1.2.xsd'
curl --output /usr/share/xml/shibboleth/WS-Trust.xsd 'https://gitlab.unige.ch/prodspub/shibboleth/-/raw/master/WS-Trust.xsd'
```

* Tester la configuration Shibboleth et redémarrer le service Shibboleth
```
shibd -t
systemctl restart shibd.service
```


* Modifier la configuration du virtual host Apache (e.g. `/etc/apache2/sites-enabled/monsite.unige.ch.conf`)
* Ajouter la référence à l'ADFS pour l'authentification comme indiqué:

```apache
<Location "/">
  AuthType shibboleth
  ShibRequestSetting entityID http://adfs.unige.ch/adfs/services/trust
  ShibRequestSetting requireSession true
  ShibUseHeaders On
  Require shibboleth
  Require valid-user
</Location>
```
* Tester la configuration Apache et redémarrer le service Apache

```
apache2ctl configtest
systemctl restart apache2.service
```

* [Soumettre une demande de déclaration de site pour la configuration SSO dans l'infrastructure UNIGE](https://support-si.unige.ch/openentry.html?tid=SRD000000004902)
